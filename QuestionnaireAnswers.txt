/**
 * File Name: ITMInterview Coding Test Question
 * Candidate Name:Emma(Shuixiu Tan)
 * Test Date:2020-11-30
 **/


1. How long did you spend on the coding test?
   I spent about 6 hour for doing this project.    
   I didn't use cakePHP before,so I read document and watched some video to study before I do the coding test. 

2. What would you add to your code or improve if you had more time?
   If I have chance,I will:
   1）  because I didnt use find funciton in search,
        now I use addslashes(mysql_real_escape_string() and mysql_escape_string() not work), 
	but I don't think it is a good way,I will search and fix it if I have more time
   2) add CUD(create,update and delete) products function. 
   4) add register and login for the project
   5) add some more pages and make it as a complete project can running for company.
   6) performance, 5k of products stil be fine but not sure if data volumn incresed, 
      what is the impact of performace, the sql below is quick attempt but need valdate with real data


3. Which part(s), if any, did you struggle with?

   1) Because the php5.4 version is old, need to spent time to set up the development environment.
   2) At the beginning,I get the products used find,but it is difficult to do "how many fields have a match" condition.
               	// $products = $this->Product->find('all',
                // array(
                // 'conditions'=>array( 
                //     'OR' => array(
                //     'model like'=>"%$searchContent%",
                //     'name like'=>"%$searchContent%",
                //     'brand_name like'=>"%$searchContent%",
                //     'description like'=>"%$searchContent%")),
                // 'order' => 'inventory DESC', 
                // 'limit' => 10,
                // 'page' =>$page
                // ));
                // $this->set('products',$products);    
	then I changed to use sql query way,then I have to do paginated again.

   3） for the search sql, I try two way to try:
	
	first one is subquery:	
	
                $products = $this->Product->query('select id, model, name, description, brand_name, inventory, 
                    match_table.id_match + match_table.model_match + match_table.name_match + match_table.description_match + match_table.brand_name_match  as match_sum from  (
                        select 
                        id, model, name, description, brand_name, inventory,
                        CASE WHEN id LIKE "%xe%" THEN 1 ELSE 0  END  as id_match,
                        CASE WHEN model LIKE "%xe%" THEN 1 ELSE 0  END  as model_match,
                        CASE WHEN name LIKE "%xe%" THEN 1 ELSE 0  END  as name_match,
                        CASE WHEN description LIKE "%xe%" THEN 1 ELSE 0  END  as description_match,
                        CASE WHEN brand_name LIKE "%xe%" THEN 1 ELSE 0  END  as brand_name_match
                        from products
                    )  as match_table
                order by match_sum desc,inventory desc   LIMIT '.$limitStart.','.$pagesize.'');   

	second one is:

		SELECT id, model, name, description, brand_name,inventory,  
                    CASE WHEN `model` LIKE "%'.$searchContent.'%" THEN 1 ELSE 0 END
                    + CASE WHEN `name` LIKE "%'.$searchContent.'%" THEN 1 ELSE 0 END
                    + CASE WHEN `description` LIKE "%'.$searchContent.'%" THEN 1 ELSE 0 END
                    + CASE WHEN `brand_name` LIKE "%'.$searchContent.'%" THEN 1 ELSE 0 END AS match_sum
                FROM products
                WHERE CONCAT_WS(model, name, description, brand_name) LIKE "%'.$searchContent.'%"
                ORDER BY
                match_sum DESC,
                inventory DESC
	I think the second will be more fast than the first when the data is large.