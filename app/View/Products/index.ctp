<div class="row">
    <form action="products" method="get">
        <div class="col-xs-12">
            <br />
            <h1>Products Management</h1>
            <br /></br />
            <input name="searchContent" maxlength="255" value="<?echo $searchContent;?>" style="width:80%;height:35px" placeholder=" Please enter to search" aria-label="Search">
            &nbsp;&nbsp;<button class="btn btn-primary" type="submit">Search</button>
            <?php if (isset($products)) { ?>
                <hr />
                <span style="margin-right:100px;">Total Records: <?php echo $totalRecords; ?> </span>
                <? if($currentPage>1){ ?>
                <a href="products?searchContent=<? echo $searchContent;?>&page=<? echo $currentPage-1;?>">Previous</a>
                <? } ?>
                &nbsp;page
                <? echo $currentPage;?>
                <? if($currentPage<(ceil($totalRecords/10))){ ?>
                &nbsp;<a href="products?searchContent=<? echo $searchContent;?>&page=<? echo $currentPage+1;?>">Next</a>
                <? } ?>
                <hr />
                <table class="table table-bordered">
                    <thead>
                        <tr style="background:#4655cf;color:white">
                            <th scope="col">#</th>
                            <th scope="col">Model</th>
                            <th scope="col">Name</th>
                            <th scope="col">Brand Name</th>
                            <th scope="col">Description</th>
                            <th scope="col">MatchFields</th>
                            <th scope="col">Inventory</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($products as $product) : ?>
                            <tr>
                                <td><?= $product['products']['id']; ?></td>
                                <td><?= $product['products']['model']; ?></td>
                                <td><?= $product['products']['name']; ?></td>
                                <td><?= $product['products']['brand_name'] ?></td>
                                <td><?= $product['products']['description'] ?></td>
                                <td><?= $product[0]['match_sum'] ?></td>
                                <td><?= $product['products']['inventory'] ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            <?php } ?>
        </div>
    </form>
</div>