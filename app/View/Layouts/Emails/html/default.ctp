<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
    <title><?php echo $title_for_layout;?></title>
</head>
<body style="background:#F6F6F6; font-family: Helvetica, Arial, sans-serif; font-size:12px; margin:0; padding:0;">
    <div style="background:#F6F6F6; font-family: Helvetica, Arial, sans-serif; font-size:12px; margin:0; padding:0;">
        <table cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr>
                <td align="center" valign="top" style="padding:10px 0 10px 0">
                    <table bgcolor="#FFFFFF" cellspacing="0" cellpadding="15" border="0" style="border:1px solid #E0E0E0; width: 650px;font-family: Helvetica, Arial, sans-serif;">

                        <tr>
                            <td valign="top">
                                &nbsp;<span style="font-size:18px;">Tin</span>
                            </td>
                        <tr>
                            <td>
                                <?php echo $content_for_layout;?>
                            </td>
                        </tr>
                        
                    </table>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
