<!DOCTYPE html>
<html lang="en">
<head>
    <?php echo $this->Html->charset(); ?>
    <title><?php echo $title_for_layout; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head> 
<body>
    <section class="navigation" style="background-color: #EFEFEF;">
        <ul class="nav nav-pills">
            <li><?php echo $this->Html->link("Home", array('action' => "index")); ?>
            <li><?php echo $this->Html->link("Products", array('controller'=>'products','action' => "index")); ?>
        </ul>
    </section>

    <section class="center">
        <div class="container content">
            <?php
                echo $this->Session->flash();
                echo $this->fetch('content');
            ?>
        </div>
    </section>

    <?php
        echo $this->element('sql_dump');
        echo $this->Js->writeBuffer(array('cache' => true));
    ?>

</body>
</html>
