<?php

/**
 * Model Name:SearchHistory
 * Database Name:itminterviewtest
 * Table Name:search_histories
 * Create Statement:
    CREATE TABLE `search_histories` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `search_content` varchar(255) NOT NULL,
        `search_time` datetime NOT NULL,
        `matched_number` int(11) NOT NULL,
        PRIMARY KEY (`id`),
        UNIQUE KEY `id_UNIQUE` (`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
 * Model Create Date:2020-11-30
 * Model Create By: Emma(Shuixiu Tan)
 **/

App::uses('AppModel', 'Model');

class SearchHistory extends AppModel
{
    public $useTable = 'search_histories';
    public $name = 'SearchHistory';
}
