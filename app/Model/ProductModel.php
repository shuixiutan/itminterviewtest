<?php

/**
 * Model Name:Product
 * Database Name:itminterview
 * Table Name:products
 * Create Statement:
     REATE TABLE `products` (
	`id` CHAR(36) NULL DEFAULT NULL,
	`model` VARCHAR(64) NULL DEFAULT NULL,
	`name` VARCHAR(255) NULL DEFAULT NULL,
	`description` TEXT(65535) NULL DEFAULT NULL,
	`brand_name` VARCHAR(128) NULL DEFAULT NULL,
	`inventory` INT(3) NOT NULL DEFAULT '0'
    )
    COLLATE='utf8_general_ci'
    ENGINE=InnoDB
    ;
 * Model Create Date:2020-11-30
 * Model Create By: Emma(Shuixiu Tan)
 **/

App::uses('AppModel', 'Model');

class Product extends AppModel
{
    public $name = 'Products';
}
