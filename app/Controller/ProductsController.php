<?php

/**
 * ProductsController:Management the products:search, add, delete, edit and so on
 * Create Date:2020-11-30
 * Author:Emma(Shuixiu Tan)
 */
App::import('Sanitize');
class ProductsController extends AppController
{
    public function index()
    {
        //check if start search and have result records or if click some pages
        if (!isset($this->request->query['searchContent']) && !isset($this->request->query['page'])) {
            return ;
        }

        $pagesize = 10;     //constant need to put to config in this project config
        $searchContent = addslashes($_GET['searchContent']);
    
        $page = isset($_GET['page']) ?  $_GET['page'] : 1;
        $limitStart =  ($page - 1) * $pagesize;

        //if no enter and click search,return,don't display anything
        if (strlen($searchContent) == 0) {
            return ;
        }

        /*
        * Get total records to calculate how many pages, because we need only the rows count by search 
        * string, we don't care about order of rows and inventory 
        */
        $totalProducts = $this->Product->find(
            'all',
            array(
                'conditions' => array(
                    'OR' => array(
                        'id like' => "%$searchContent%",
                        'model like' => "%$searchContent%",
                        'name like' => "%$searchContent%",
                        'brand_name like' => "%$searchContent%",
                        'description like' => "%$searchContent%"
                    )
                )
            )
        );
        $totalRecords = sizeof($totalProducts);

        //get one page products records to display
        $products = $this->Product->query('
            SELECT id, model, name, description, brand_name,inventory,  
                    CASE WHEN `id` LIKE "%' . $searchContent . '%" THEN 1 ELSE 0 END
                + CASE WHEN `model` LIKE "%' . $searchContent . '%" THEN 1 ELSE 0 END
                + CASE WHEN `name` LIKE "%' . $searchContent . '%" THEN 1 ELSE 0 END
                + CASE WHEN `description` LIKE "%' . $searchContent . '%" THEN 1 ELSE 0 END
                + CASE WHEN `brand_name` LIKE "%' . $searchContent . '%" THEN 1 ELSE 0 END AS match_sum
            FROM products
            WHERE CONCAT_WS(id, model, name, description, brand_name) LIKE "%' . $searchContent . '%"
            ORDER BY
            match_sum DESC,
            inventory DESC  LIMIT ' . $limitStart . ',' . $pagesize . '');
        $this->set('products', $products);
        $this->set('totalRecords', $totalRecords);
        $this->set('searchContent', $searchContent);
        $this->set('currentPage', $page);

        // inseart a record to search_histories table, only insert after click search button
        if ($page == 1) {
            $this->saveSearcHistory($searchContent, $totalRecords);
        }
    }

    private function saveSearcHistory($searchContent, $totalRecords)
    {
        $this->loadModel('SearchHistory');
        $historyData = array(
            'SearchHistory' => array(
                'search_content' => $searchContent,
                'search_time' => date("Y-m-d H:i:s"),
                'matched_number' => $totalRecords
            )
        );
        $this->SearchHistory->save($historyData);
    }
}
