# ITM Instruments - Software Development Interview Test

Please complete the Task outlined below & answer the short Questionnaire at the end.

To submit your solution, ZIP it in a folder named `{yourname}.zip`, upload the ZIP file to a shared Google Drive folder & send us the link to access it.

**Task:**  
Set up a Search Bar to find products in the table & data provided in the `products.sql` file.

- The Search should check for matches in the data provided & rank them based on how many fields have a match & the inventory we have (highest to lowest).
- The Search results should be paginated, with a maximum of 10 products per page.
- The Search should track every search made & the number of products it matched and save that information to a database table.

**Requirements:**  
- The code must be written in PHP 5.4 and MySQL.
- The solution must use the framework & code provided in this folder as a starting point (CakePHP 2.4 - https://book.cakephp.org/2/en/index.html). 
- Greater focus should be put on the Back-End than the Front-End. The Front-End can be kept very simple (Bootstrap 3.3 is included for the View).
- For any new tables you create, the MySQL CREATE statement must be included in a comment at the top of the Model's file.


**Questionnaire**  
After completing the test, answer the following questions in a text file named "QuestionnaireAnswers.txt" and include it in your ZIP file.

1. How long did you spend on the coding test?
2. What would you add to your code or improve if you had more time?
3. Which part(s), if any, did you struggle with?
 
